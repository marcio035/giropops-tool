#!/usr/bin/env python

import argparse
from convert import get_from_db
from create_list import create

parser = argparse.ArgumentParser(description='Video converter tool for LINUXtips videos')
parser.set_defaults(func=lambda args: parser.print_help())

subparsers = parser.add_subparsers()

convert = subparsers.add_parser("convert", help='Used to convert videos to a small size with same quality')
#convert.add_argument("--path", type=str, help='Need to pass the directory of the videos that will be converted')
convert.set_defaults(func=get_from_db)

create_list = subparsers.add_parser("list", help='Used to create a list of videos to convert, and test the integrity of the video.')
create_list.add_argument("create", type=str, help='Need to pass the directory of the videos that will be converted')
create_list.add_argument("--path", type=str, help='Need to pass the directory of the videos that will be converted')
create_list.set_defaults(func=create)

args = parser.parse_args()

args.func(args)